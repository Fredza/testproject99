FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /
COPY ["TestSolution.sln", "TestSolution.sln"]
COPY ["src/Services/Test/Test.Api/Test.Api.csproj", "src/Services/Test/Test.Api/"]
COPY ["src/Services/Test/Test.Domain/Test.Domain.csproj", "src/Services/Test/Test.Domain/"]
COPY ["src/Services/Test/Test.Infrastructure/Test.Infrastructure.csproj", "src/Services/Test/Test.Infrastructure/"]
COPY ["test/Test.Tests/Test.Tests.csproj", "test/Test.Tests/"]

RUN dotnet restore "/TestSolution.sln"
COPY . .
WORKDIR "/src/Services/Test/Test.Api/"
RUN dotnet build "Test.Api.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet test "/test/Test.Tests/Test.Tests.csproj"
RUN dotnet publish "Test.Api.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Test.Api.dll"]



##WORKDIR /src
##COPY ["TestSolution.sln", "TestSolution.sln"]
##COPY ["Test.Api/Test.Api.csproj", "Test.Api/"]
##COPY ["Test.Domain/Test.Domain.csproj", "Test.Domain/"]
##COPY ["Test.Tests/Test.Tests.csproj", "Test.Tests/"]
##RUN dotnet restore "/src/TestSolution.sln"
##COPY . .
##WORKDIR "/src/Test.Api"
##RUN dotnet build "Test.Api.csproj" -c Release -o /app
##
##FROM build AS publish
##RUN dotnet test "/src/Test.Tests/Test.Tests.csproj"
##RUN dotnet publish "Test.Api.csproj" -c Release -o /app
