using Test.Domain;
using Xunit;

namespace Test.Tests
{
   public class UnitTest1
   {
      [Fact]
      public void Test1()
      {
         Assert.False(false);
      }

      [Fact]
      public void Test2()
      {
         // Arrange
         var expected = "11";
         Class1 sut = new Class1();

         // Act
         var actual = sut.Add(9, 3);

         // assert
         Assert.Equal(expected, actual);
      }
      
      [Fact]
      public void Test3()
      {
         Assert.False(false);
      }

   }
}
